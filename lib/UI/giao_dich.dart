import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:flutter/material.dart' as prefix0;

class GiaoDich extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => GiaoDichpage();
}

class GiaoDichpage extends State<GiaoDich> {
  @override
  Widget build(BuildContext context) {

    var list = [
      {
        'id':0,
        'date':'11/2019'
      },
      {
        'id': 1,
        'avatar':
            'http://vn.meet-magento.com/wp-content/uploads/2015/08/Logo-MOMO-d%C6%B0%C6%A1ng-b%E1%BA%A3n-01.jpg',
        'title': 'nhận được tiền hoa hồng',
        'date': '20/11',
        'status': 'thành công',
        'money': 2000.0,
        'd':'đ',
        'calculation':'+'
      },
      {
        'id': 2,
        'avatar': 'https://img.lovepik.com/element/40025/2707.png_1200.png',
        'title': 'mua mã thẻ di động viettel',
        'date': '20/11',
        'status': 'thành công',
        'money': 50000.0,
        'd':'đ',
        'calculation':'-'
      },
      {
        'id': 3,
        'avatar':
            'https://png.pngtree.com/png-clipart/20190614/original/pngtree-mobile-banking-icon-png-image_3697641.jpg',
        'title': 'nạp tiền vào ví từ BIDV',
        'date': '20/11',
        'status': 'thành công',
        'money': 75000.0,
        'd':'đ',
        'calculation':'+'
      },
      {
        'id': 4,
        'avatar': 'https://img.lovepik.com/element/40025/2707.png_1200.png',
        'title': 'mua mã thẻ di động viettel',
        'date': '20/11',
        'status': 'thành công',
        'money': 50000.0,
        'd':'đ',
        'calculation':'-'
      },
      {
        'id': 5,
        'avatar':
            'http://vn.meet-magento.com/wp-content/uploads/2015/08/Logo-MOMO-d%C6%B0%C6%A1ng-b%E1%BA%A3n-01.jpg',
        'title': 'nhận được tiền hoa hồng',
        'date': '20/11',
        'status': 'thành công',
        'money': 2000.0,
        'd':'đ',
        'calculation':'+'
      },
      {
        'id': 6,
        'avatar':
            'https://png.pngtree.com/png-clipart/20190614/original/pngtree-mobile-banking-icon-png-image_3697641.jpg',
        'title': 'nạp tiền vào ví từ BIDV',
        'date': '20/11',
        'status': 'thành công',
        'money': 400000.0,
        'd':'đ',
        'calculation':'+'
      },
      {
        'id': 7,
        'avatar':
            'http://vn.meet-magento.com/wp-content/uploads/2015/08/Logo-MOMO-d%C6%B0%C6%A1ng-b%E1%BA%A3n-01.jpg',
        'title': 'nhận được tiền hoa hồng',
        'date': '20/11',
        'status': 'thành công',
        'money': 2000.0,
        'd':'đ',
        'calculation':'+'
      },
      {
        'id': 8,
        'avatar': 'https://img.lovepik.com/element/40025/2707.png_1200.png',
        'title': 'mua mã thẻ di động viettel',
        'date': '20/11',
        'status': 'thành công',
        'money': 50000.0,
        'd':'đ',
        'calculation':'-'
      },
      {
        'id': 9,
        'avatar': 'https://img.lovepik.com/element/40025/2707.png_1200.png',
        'title': 'mua mã thẻ di động viettel',
        'date': '20/11',
        'status': 'thành công',
        'money': 50000.0,
        'd':'đ',
        'calculation':'-'
      },
    ];
    return ListView.builder(
        itemCount: list.length,
        itemBuilder: (BuildContext contex, int index) {
          return list[index]['id'] ==0 ? Container(
            alignment: Alignment.centerLeft,
            height: 24.0,
            width: double.infinity,
            color: Colors.grey,
            child: Text('Tháng'+ list[index]['date']),
          ) : Padding(
            padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
            child: Container(
                width: double.infinity,
                height: 72.0,
                decoration: BoxDecoration(
                    border:
                        Border(bottom: BorderSide(color: Colors.grey[300]))),
                alignment: Alignment.center,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 50.0,
                      height: 50.0,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey[300]),
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(list[index]['avatar']),
                          )),
                    ),
                    Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(8.0, 0, 0, 0),
                          child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                          Text(list[index]['title']),
                          Text(list[index]['date']),
                          Text(
                            list[index]['status'],
                            style: prefix0.TextStyle(color: Colors.green),
                          ),
                      ],
                    ),
                        )),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Row(
                        children: <Widget>[Text(list[index]['calculation']) ,Text( Fomat(list[index]['money']) ), Text(list[index]['d'])],
                      )

                    )
                  ],
                )),
          );
        });
  }
  String Fomat(double number){
    FlutterMoneyFormatter fmf = FlutterMoneyFormatter(amount: number);
      return fmf.output.nonSymbol;
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
