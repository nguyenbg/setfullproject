import 'package:flutter/material.dart';
import 'dart:ui';

import 'package:flutter_money_formatter/flutter_money_formatter.dart';
class TrangChu extends StatefulWidget{
  @override
  State<StatefulWidget> createState()=> TrangChuPage();

}
class TrangChuPage extends State<TrangChu> {
  @override
  Widget build(BuildContext context) {
    FlutterMoneyFormatter fmf = FlutterMoneyFormatter(amount: 180307000.0);

    return Stack(
      children: <Widget>[
        Container(
          width: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(
                    'https://media.cungcau.vn/files/huynhnga/2019/09/18/zx-1233.jpg'),
              )),
        ),
        BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          child: Container(color: Colors.black.withOpacity(0)),
        ),
        SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CircleAvatar(
                      radius: 25.0,
                      backgroundImage: NetworkImage(
                          'https://i.pinimg.com/originals/be/5a/7a/be5a7ab5782f23066d2a8c4b54d6a105.jpg'),
                      backgroundColor: Colors.transparent,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      'Số dư :' + fmf.output.nonSymbol,
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Stack(
                          children: <Widget>[
                            Padding(
                              padding:
                              const EdgeInsets.fromLTRB(0, 8, 5, 0),
                              child: Icon(
                                Icons.message,
                                color: Colors.white,
                                size: 24,
                              ),
                            ),
                            Positioned(
                                top: -1,
                                left: 6,
                                child: CircleAvatar(
                                  radius: 9,
                                  backgroundColor: Colors.red,
                                  child: Text(
                                    '2',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12),
                                  ),
                                ))
                          ],
                        )),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(18, 16, 0, 0),
                        child: Image.asset(
                          'image/credit.png',
                          width: 50.0,
                          color: Colors.grey[200],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(18, 0, 0, 10),
                        child: Text(
                          "Thanh toan",
                          style: TextStyle(color: Colors.white),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
                        child: Image.asset(
                          'image/qr-code.png',
                          width: 50.0,
                          color: Colors.grey[200],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                        child: Text(
                          "Mã của tôi ",
                          style: TextStyle(color: Colors.white),
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 16, 18, 0),
                        child: Image.asset(
                          'image/connections.png',
                          width: 50.0,
                          color: Colors.grey[200],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 18, 10),
                        child: Text(
                          "Liên kết",
                          style: TextStyle(color: Colors.white),
                        ),
                      )
                    ],
                  ),
                ],
              ),
              Container(
                width: double.infinity,
                color: Colors.white,
                child: Wrap(
                  alignment: WrapAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.center,
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                        child: Column(
                          children: <Widget>[
                            Image.asset(
                              'image/data-transfer.png',
                              width: 36,
                              height: 36,
                            ),
                            Text(
                              'chuyển tiền',
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                        child: Column(
                          children: <Widget>[
                            Image.asset(
                              'image/ruttien.png',
                              width: 36,
                              height: 36,
                            ),
                            Text(
                              'rút tiền',
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                        child: Column(
                          children: <Widget>[
                            Image.asset(
                              'image/power-bank.png',
                              width: 36,
                              height: 36,
                            ),
                            Text(
                              'nạp tiền',
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                        child: Column(
                          children: <Widget>[
                            Image.asset(
                              'image/loan.png',
                              width: 36,
                              height: 36,
                            ),
                            Text(
                              'vay tiền',
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                        child: Column(
                          children: <Widget>[
                            Image.asset(
                              'image/newspaper.png',
                              width: 36,
                              height: 36,
                            ),
                            Text(
                              'tin tức',
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                height: 80.0,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(
                            'https://www.thuengay.vn/uploads/320x180/d614b75ec9fd1327652031a465b39dad38ac9ec82.jpg'))),
              ),
              Container(
                width: double.infinity,
                color: Colors.white,
                alignment: Alignment.center,
                child: Wrap(
                  crossAxisAlignment: WrapCrossAlignment.end,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.center,
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                        child: Column(
                          children: <Widget>[
                            Image.asset(
                              'image/data-transfer.png',
                              width: 36,
                              height: 36,
                            ),
                            Text(
                              'chuyển tiền',
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                        child: Column(
                          children: <Widget>[
                            Image.asset(
                              'image/ruttien.png',
                              width: 36,
                              height: 36,
                            ),
                            Text(
                              'rút tiền',
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                        child: Column(
                          children: <Widget>[
                            Image.asset(
                              'image/power-bank.png',
                              width: 36,
                              height: 36,
                            ),
                            Text(
                              'nạp tiền',
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                        child: Column(
                          children: <Widget>[
                            Image.asset(
                              'image/loan.png',
                              width: 36,
                              height: 36,
                            ),
                            Text(
                              'vay tiền',
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                        child: Column(
                          children: <Widget>[
                            Image.asset(
                              'image/newspaper.png',
                              width: 36,
                              height: 36,
                            ),
                            Text(
                              'tin tức',
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                        child: Column(
                          children: <Widget>[
                            Image.asset(
                              'image/data-transfer.png',
                              width: 36,
                              height: 36,
                            ),
                            Text(
                              'chuyển tiền',
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                        child: Column(
                          children: <Widget>[
                            Image.asset(
                              'image/ruttien.png',
                              width: 36,
                              height: 36,
                            ),
                            Text(
                              'rút tiền',
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                        child: Column(
                          children: <Widget>[
                            Image.asset(
                              'image/power-bank.png',
                              width: 36,
                              height: 36,
                            ),
                            Text(
                              'nạp tiền',
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                        child: Column(
                          children: <Widget>[
                            Image.asset(
                              'image/loan.png',
                              width: 36,
                              height: 36,
                            ),
                            Text(
                              'vay tiền',
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 25, 0, 0),
                        child: Column(
                          children: <Widget>[
                            Image.asset(
                              'image/newspaper.png',
                              width: 36,
                              height: 36,
                            ),
                            Text(
                              'tin tức',
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
@override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

}