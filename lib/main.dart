import 'dart:ui';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:flutter/material.dart';
import 'package:animated_splash/animated_splash.dart';
import 'package:cuberto_bottom_bar/cuberto_bottom_bar.dart';
import 'package:userface/UI/giao_dich.dart';
import 'package:userface/UI/trang_chu.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: AnimatedSplash(
          imagePath: 'image/spl1ash.gif',
          home: MyHomePage(title: 'Flutter Demo Home Page'),
          duration: 4500,
          type: AnimatedSplashType.StaticDuration,
        ));
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  int _page = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: CubertoBottomBar(
          tabStyle: CubertoTabStyle.STYLE_FADED_BACKGROUND,
          initialSelection: 0,
          tabs: [
            TabData(
                iconData: Icons.home,
                title: "Trang chủ",
                tabColor: Colors.deepPurple),
            TabData(
                iconData: Icons.credit_card,
                title: "Giao dịch",
                tabColor: Colors.pink),
            TabData(
                iconData: Icons.business_center,
                title: "Ưu đãi",
                tabColor: Colors.amber),
            TabData(
                iconData: Icons.perm_identity,
                title: "Tài khoản",
                tabColor: Colors.teal),
          ],
          onTabChangedListener: (position, title, color) {
            setState(() {
              _page = position;
            });
          },
        ),
        body: SafeArea(
          child: menu(_page),
        ));
  }

  menu(_page) {
    switch (_page) {
      case 0:
        {
          return TrangChu();
        }
        break;

      case 1:
        {
          return GiaoDich();
        }
        break;

      case 2:
        {
          return Text('2');
        }
        break;
      case 3:
        {
          return Text('3');
        }
        break;
    }
  }

}
